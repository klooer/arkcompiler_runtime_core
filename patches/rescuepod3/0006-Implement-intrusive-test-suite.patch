From cb246f149e0f6170bb2c3db6971f1acfa6390825 Mon Sep 17 00:00:00 2001
From: Evgeny Gerlits <>
Date: Tue, 31 May 2022 02:35:39 +0800
Subject: [PATCH 6/7] Implement intrusive test suite TicketNo:_internal_
 Description:Intrusive testing is a regression testing method to detect
 multithreading issues in source code like data races. Implemented: - source
 code instrumentation subsystem; - parser of test YAML-files with mapping from
 synchronization points to synchronization actions; - specification of several
 synchronization points in ARK source code (for one test at least); -
 implementation of the synchronization points for one test - source code
 module  with parameterized functions, performing synchronization actions on
 the base of synchronization point context and state of the test; - mapping of
 the implemented synchronization points - yaml module mapping the
 synchronization points specifications to its implementations; - integration
 of the intrusive test suite into the continous integration subsytem.
 Team:ARK Feature or Bugfix:Feature Binary Source:No PrivateCode(Yes/No):No

Change-Id: Id5037239a9a7a9624264fe0149b7932aa495e2d3
---
 runtime/CMakeLists.txt                  |  8 ++++++
 runtime/include/intrusive_test.h        | 38 +++++++++++++++++++++++++
 runtime/include/intrusive_test_option.h | 33 +++++++++++++++++++++
 runtime/include/managed_thread.h        |  4 +++
 runtime/include/mtmanaged_thread.h      | 16 +++++++++++
 runtime/intrusive_test.cpp              | 20 +++++++++++++
 runtime/options.yaml                    |  5 ++++
 runtime/runtime.cpp                     | 23 +++++++++++++++
 runtime/thread_manager.cpp              |  4 +++
 9 files changed, 151 insertions(+)
 create mode 100644 runtime/include/intrusive_test.h
 create mode 100644 runtime/include/intrusive_test_option.h
 create mode 100644 runtime/intrusive_test.cpp

diff --git a/runtime/CMakeLists.txt b/runtime/CMakeLists.txt
index fdf2aae1e2..b5512c7996 100644
--- a/runtime/CMakeLists.txt
+++ b/runtime/CMakeLists.txt
@@ -20,6 +20,7 @@ enable_language(ASM)
 include(core/Core.cmake)
 include(${PANDA_ROOT}/verification/Verification.cmake)
 
+
 set(SOURCES
     assert_gc_scope.cpp
     bridge/bridge.cpp
@@ -126,6 +127,13 @@ set(SOURCES
     loadable_agent.cpp
 )
 
+if (INTRUSIVE_TESTING)
+    set(IS_RUNTIME_INTRUSIVE_BUILD true)
+    list(APPEND SOURCES intrusive_test.cpp)
+    panda_set_flag(INTRUSIVE_TESTING)
+    add_subdirectory(${PANDA_ROOT}/plugins/java/tests/intrusive-tests ${CMAKE_BINARY_DIR}/plugins/java/tests/intrusive_tests/runtime)
+endif()
+
 if(PANDA_TARGET_ARM32_ABI_SOFT OR PANDA_TARGET_ARM32_ABI_SOFTFP)
     list(APPEND SOURCES
         bridge/arch/arm/interpreter_to_compiled_code_bridge_arm.S
diff --git a/runtime/include/intrusive_test.h b/runtime/include/intrusive_test.h
new file mode 100644
index 0000000000..2689d5b23b
--- /dev/null
+++ b/runtime/include/intrusive_test.h
@@ -0,0 +1,38 @@
+/*
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+#ifndef PANDA_INTRUSIVE_TEST_H
+#define PANDA_INTRUSIVE_TEST_H
+
+#include <cstdint>
+
+namespace panda {
+class IntrusiveTest {
+protected:
+    static uint32_t id;
+
+public:
+    static void set_id(uint32_t test_id)
+    {
+        IntrusiveTest::id = test_id;
+    }
+
+    static uint32_t get_id()
+    {
+        return IntrusiveTest::id;
+    }
+};
+}  // namespace panda
+
+#endif  // PANDA_INTRUSIVE_TEST_H
diff --git a/runtime/include/intrusive_test_option.h b/runtime/include/intrusive_test_option.h
new file mode 100644
index 0000000000..5f1d6ee998
--- /dev/null
+++ b/runtime/include/intrusive_test_option.h
@@ -0,0 +1,33 @@
+/*
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+#ifndef PANDA_INTRUSIVE_TEST_OPTION_H
+#define PANDA_INTRUSIVE_TEST_OPTION_H
+
+#include "runtime/include/runtime_options.h"
+#include "runtime/include/intrusive_test.h"
+
+namespace panda {
+class IntrusiveTestOption {
+public:
+    static void set_test_id([[maybe_unused]] const RuntimeOptions &options)
+    {
+#if defined(INTRUSIVE_TESTING)
+        IntrusiveTest::set_id(options.GetIntrusiveTest());
+#endif
+    }
+};
+}  // namespace panda
+
+#endif  // PANDA_INTRUSIVE_TEST_OPTION_H
diff --git a/runtime/include/managed_thread.h b/runtime/include/managed_thread.h
index 1fee98a108..ebe02e8c41 100644
--- a/runtime/include/managed_thread.h
+++ b/runtime/include/managed_thread.h
@@ -646,6 +646,10 @@ public:
         PrintSuspensionStackIfNeeded();
         UpdateStatus(ThreadStatus::IS_SUSPENDED);
         {
+            /* @sync 1
+             * @description Right after the thread updates its status to IS_SUSPENDED and right before beginning to wait
+             * for actual suspension
+             */
             os::memory::LockHolder lock(suspend_lock_);
             while (suspend_count_ > 0) {
                 suspend_var_.TimedWait(&suspend_lock_, TIMEOUT);
diff --git a/runtime/include/mtmanaged_thread.h b/runtime/include/mtmanaged_thread.h
index 4b09bc7ee7..4636ba5410 100644
--- a/runtime/include/mtmanaged_thread.h
+++ b/runtime/include/mtmanaged_thread.h
@@ -227,6 +227,9 @@ public:
         {
             os::memory::LockHolder lock(cond_lock_);
             UpdateStatus(wait_status);
+            /* @sync 1
+             * @description Right after changing the thread's status and before going to sleep
+             * */
             res = TimedWaitWithLockHeldInternal(timeout, nanos, is_absolute);
         }
         UpdateStatus(old_status);
@@ -241,12 +244,25 @@ public:
     void TerminationLoop()
     {
         ASSERT(IsRuntimeTerminated());
+        /* @sync 1
+         * @description This point is right before the thread starts to release all his monitors.
+         * All monitors should be released by the thread before completing its execution by stepping into the
+         * termination loop.
+         * */
         if (GetStatus() == ThreadStatus::NATIVE) {
             // There is a chance, that the runtime will be destroyed at this time.
             // Thus we should not release monitors for NATIVE status
         } else {
             ReleaseMonitors();
+            /* @sync 2
+             * @description This point is right after the thread has released all his monitors and right before it steps
+             * into the termination loop.
+             * */
             UpdateStatus(ThreadStatus::IS_TERMINATED_LOOP);
+            /* @sync 3
+             * @description This point is right after the thread has released all his monitors and changed status to
+             * IS_TERMINATED_LOOP
+             * */
         }
         while (true) {
             static constexpr unsigned int LONG_SLEEP_MS = 1000000;
diff --git a/runtime/intrusive_test.cpp b/runtime/intrusive_test.cpp
new file mode 100644
index 0000000000..2d40ece539
--- /dev/null
+++ b/runtime/intrusive_test.cpp
@@ -0,0 +1,20 @@
+/*
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+#include "runtime/include/intrusive_test.h"
+
+#include <cstdint>
+
+uint32_t panda::IntrusiveTest::id;
diff --git a/runtime/options.yaml b/runtime/options.yaml
index adb58c4ff5..5902c5ec79 100755
--- a/runtime/options.yaml
+++ b/runtime/options.yaml
@@ -721,3 +721,8 @@ options:
   type: std::string
   default: "/data/profile/"
   description: Specify the location of abc layout profile
+
+- name: intrusive-test
+  type: uint32_t
+  default: 0
+  description: Numerical identifier of an intrusive test
diff --git a/runtime/runtime.cpp b/runtime/runtime.cpp
index 68a5c7ca43..155d599eac 100644
--- a/runtime/runtime.cpp
+++ b/runtime/runtime.cpp
@@ -76,6 +76,7 @@
 #include "verification/jobs/cache.h"
 #include "verification/jobs/thread_pool.h"
 #include "verification/type/type_systems.h"
+#include "runtime/include/intrusive_test_option.h"
 
 namespace panda {
 
@@ -319,6 +320,8 @@ bool Runtime::Create(const RuntimeOptions &options)
         return false;
     }
 
+    IntrusiveTestOption::set_test_id(options);
+
     const_cast<RuntimeOptions &>(options).InitializeRuntimeSpacesAndType();
     trace::ScopedTrace scoped_trace("Runtime::Create");
 
@@ -411,7 +414,14 @@ bool Runtime::Destroy()
 
     instance->GetNotificationManager()->VmDeathEvent();
 
+    /* @sync 1
+     * @description Before starting to unitialize threads
+     * */
     instance->GetPandaVM()->UninitializeThreads();
+    /* @sync 2
+     * @description After uninitialization of threads all deamon threads should have gone into the termination loop and
+     * all other threads should have finished.
+     * */
     // Stop GC after UninitializeThreads because
     // UninitializeThreads may execute managed code which
     // uses barriers
@@ -483,6 +493,9 @@ std::string GetMainRuntimeType(const RuntimeOptions &options)
         return options.GetRuntimeType();
     }
 
+    /* @sync 1
+     * @description Right before setting runtime options in Runtime constructor
+     */
     std::vector<std::string> load_runtimes = options.GetLoadRuntimes();
     for (const std::string &str_runtime : load_runtimes) {
         // Choose first non-core runtime.
@@ -501,6 +514,9 @@ Runtime::Runtime(const RuntimeOptions &options, mem::InternalAllocatorPtr intern
       cha_(new ClassHierarchyAnalysis),
       zygote_no_threads_(false)
 {
+    /* @sync 1
+     * @description Right before setting runtime options in Runtime constructor
+     */
     Runtime::runtime_type_ = GetMainRuntimeType(options);
     Runtime::options_ = options;
 
@@ -540,6 +556,9 @@ Runtime::Runtime(const RuntimeOptions &options, mem::InternalAllocatorPtr intern
 #ifdef PANDA_ENABLE_RELAYOUT_PROFILE
     relayout_profiler_ = internal_allocator_->New<RelayoutProfiler>();
 #endif
+    /* @sync 2
+     * @description At the very end of the Runtime's constructor when all initialization actions have completed.
+     * */
 }
 
 Runtime::~Runtime()
@@ -575,6 +594,10 @@ Runtime::~Runtime()
         internal_allocator_->Delete(relayout_profiler_);
     }
 #endif
+    /* @sync 1
+     * @description Right after runtime's destructor has deleted panda virtual machine
+     * */
+
     // crossing map is shared by different VMs.
     mem::CrossingMapSingleton::Destroy();
 
diff --git a/runtime/thread_manager.cpp b/runtime/thread_manager.cpp
index 94b585128c..2fe1d9fad2 100644
--- a/runtime/thread_manager.cpp
+++ b/runtime/thread_manager.cpp
@@ -185,6 +185,10 @@ void ThreadManager::StopDaemonThreads() REQUIRES(thread_lock_)
     for (auto thread : threads_) {
         if (thread->IsDaemon()) {
             LOG(DEBUG, RUNTIME) << "Stopping daemon thread " << thread->GetId();
+            /* @sync 1
+             * @description The thread manager will request the daemon thread to go into the termination loop after this
+             * point.
+             * */
             thread->StopDaemonThread();
         }
     }
-- 
2.17.1

