From f732379ff1ee3e9456a0cf698960435396383cfc Mon Sep 17 00:00:00 2001
From: Robert Fancsik <>
Date: Mon, 13 Jun 2022 17:12:15 +0200
Subject: [PATCH 22/29] Revive tests

TicketNo:_internal_
Description:Fix several compiler/runtime bugs

Team:ARK
Feature or Bugfix:Bugfix
Binary Source:No
PrivateCode(Yes/No):No

Change-Id: Icc7f7da9fa3678d7bb009003cd59a4cf6a1b6c03
---
 plugins/ecmascript/es2panda/binder/binder.cpp |  7 +-
 plugins/ecmascript/es2panda/binder/binder.h   | 20 +++--
 plugins/ecmascript/es2panda/binder/scope.cpp  | 10 +--
 .../es2panda/compiler/base/literals.h         |  3 +-
 .../es2panda/compiler/base/lreference.cpp     |  4 +
 .../es2panda/compiler/core/compilerContext.h  | 17 ++--
 .../es2panda/compiler/core/compilerImpl.cpp   |  8 +-
 .../es2panda/compiler/core/emitter.cpp        | 85 ++-----------------
 .../es2panda/compiler/core/emitter.h          |  8 +-
 .../es2panda/compiler/core/function.cpp       | 13 +--
 .../es2panda/compiler/core/pandagen.cpp       |  6 +-
 .../es2panda/compiler/core/pandagen.h         |  2 +-
 .../es2panda/compiler/core/regAllocator.h     |  2 +-
 .../es2panda/compiler/templates/isa.h.erb     |  2 +-
 .../es2panda/ir/base/classDefinition.cpp      | 55 +++++++-----
 .../es2panda/ir/base/classDefinition.h        |  3 +-
 .../es2panda/ir/expressions/identifier.h      |  5 ++
 .../ir/expressions/memberExpression.cpp       |  7 ++
 plugins/ecmascript/isa/isa.yaml               | 10 ++-
 .../runtime/class_info_extractor.cpp          |  2 +-
 .../class_linker/panda_file_translator.cpp    |  2 +-
 .../class_linker/panda_file_translator.h      |  1 -
 plugins/ecmascript/runtime/ecma_runtime.yaml  |  2 +-
 .../interpreter/ecma-interpreter-inl.h        |  9 +-
 .../runtime/interpreter/interpreter.h         |  2 +-
 plugins/ecmascript/runtime/intrinsics-inl.h   | 18 ++--
 26 files changed, 138 insertions(+), 165 deletions(-)

diff --git a/plugins/ecmascript/es2panda/binder/binder.cpp b/plugins/ecmascript/es2panda/binder/binder.cpp
index 485ae5eabb..c2a2113362 100644
--- a/plugins/ecmascript/es2panda/binder/binder.cpp
+++ b/plugins/ecmascript/es2panda/binder/binder.cpp
@@ -582,15 +582,16 @@ void Binder::ResolveReferences(const ir::AstNode *parent)
 
 LocalVariable *Binder::AddMandatoryParam(const std::string_view &name)
 {
-    ASSERT(scope_->IsFunctionVariableScope());
+    ASSERT(scope_->IsFunctionParamScope());
 
     auto *decl = Allocator()->New<ParameterDecl>(name);
     auto *param = Allocator()->New<LocalVariable>(decl, VariableFlags::VAR);
 
-    auto &funcParams = scope_->AsFunctionVariableScope()->ParamScope()->Params();
+    auto &funcParams = scope_->AsFunctionParamScope()->Params();
 
     funcParams.insert(funcParams.begin(), param);
-    scope_->AsFunctionVariableScope()->Bindings().insert({decl->Name(), param});
+    scope_->AsFunctionParamScope()->GetFunctionScope()->Bindings().insert({decl->Name(), param});
+    scope_->Bindings().insert({decl->Name(), param});
 
     return param;
 }
diff --git a/plugins/ecmascript/es2panda/binder/binder.h b/plugins/ecmascript/es2panda/binder/binder.h
index 6c1d9ee90d..6c06e71f70 100644
--- a/plugins/ecmascript/es2panda/binder/binder.h
+++ b/plugins/ecmascript/es2panda/binder/binder.h
@@ -173,14 +173,6 @@ private:
     ResolveBindingOptions bindingOptions_;
 };
 
-template <size_t N>
-void Binder::AddMandatoryParams(const MandatoryParams<N> &params)
-{
-    for (auto iter = params.rbegin(); iter != params.rend(); iter++) {
-        AddMandatoryParam(*iter);
-    }
-}
-
 template <typename T>
 class LexicalScope {
 public:
@@ -248,6 +240,18 @@ private:
     VariableScope *prevVarScope_ {};
 };
 
+template <size_t N>
+void Binder::AddMandatoryParams(const MandatoryParams<N> &params)
+{
+    ASSERT(scope_->IsFunctionVariableScope());
+
+    auto scopeCtx = LexicalScope<FunctionParamScope>::Enter(this, scope_->AsFunctionVariableScope()->ParamScope());
+
+    for (auto iter = params.rbegin(); iter != params.rend(); iter++) {
+        AddMandatoryParam(*iter);
+    }
+}
+
 template <typename T, typename... Args>
 T *Binder::AddTsDecl(const lexer::SourcePosition &pos, Args &&... args)
 {
diff --git a/plugins/ecmascript/es2panda/binder/scope.cpp b/plugins/ecmascript/es2panda/binder/scope.cpp
index 5b9a52fcae..a3710164f2 100644
--- a/plugins/ecmascript/es2panda/binder/scope.cpp
+++ b/plugins/ecmascript/es2panda/binder/scope.cpp
@@ -106,7 +106,7 @@ ScopeFindResult Scope::Find(const util::StringView &name, ResolveBindingOptions
         level++;
         auto *funcVariableScope = iter->AsFunctionParamScope()->GetFunctionScope();
 
-        if (funcVariableScope->NeedLexEnv()) {
+        if (funcVariableScope && funcVariableScope->NeedLexEnv()) {
             lexLevel++;
         }
 
@@ -242,7 +242,7 @@ void VariableScope::CheckDirectEval(compiler::CompilerContext *compilerCtx)
         }
     }
 
-    std::vector<const compiler::Literal *> literals(LexicalSlots() + constBindings);
+    std::vector<compiler::Literal> literals(LexicalSlots() + constBindings);
 
     if (!constBindings) {
         for (const auto &[name, variable] : bindings_) {
@@ -250,7 +250,7 @@ void VariableScope::CheckDirectEval(compiler::CompilerContext *compilerCtx)
                 continue;
             }
 
-            literals[variable->AsLocalVariable()->LexIdx()] = new compiler::Literal(name);
+            literals[variable->AsLocalVariable()->LexIdx()] = compiler::Literal(name);
         }
     } else {
         std::vector<binder::Variable *> bindings(LexicalSlots());
@@ -267,9 +267,9 @@ void VariableScope::CheckDirectEval(compiler::CompilerContext *compilerCtx)
         uint32_t buffIndex = 0;
         for (const auto *variable : bindings) {
             if (variable->Declaration()->IsConstDecl()) {
-                literals[buffIndex++] = new compiler::Literal(true);
+                literals[buffIndex++] = compiler::Literal(true);
             }
-            literals[buffIndex++] = new compiler::Literal(variable->Name());
+            literals[buffIndex++] = compiler::Literal(variable->Name());
         }
     }
 
diff --git a/plugins/ecmascript/es2panda/compiler/base/literals.h b/plugins/ecmascript/es2panda/compiler/base/literals.h
index 4a2b6e21e8..a4405e546a 100644
--- a/plugins/ecmascript/es2panda/compiler/base/literals.h
+++ b/plugins/ecmascript/es2panda/compiler/base/literals.h
@@ -107,7 +107,7 @@ public:
     std::string GetMethod() const
     {
         ASSERT(tag_ == LiteralTag::ACCESSOR || tag_ == LiteralTag::METHOD || tag_ == LiteralTag::GENERATOR_METHOD ||
-               tag_ == LiteralTag::ASYNC_GENERATOR_METHOD);
+               tag_ == LiteralTag::ASYNC_METHOD || tag_ == LiteralTag::ASYNC_GENERATOR_METHOD);
 
         return std::get<std::string>(value_);
     }
@@ -137,4 +137,3 @@ public:
 }  // namespace panda::es2panda::compiler
 
 #endif
-
diff --git a/plugins/ecmascript/es2panda/compiler/base/lreference.cpp b/plugins/ecmascript/es2panda/compiler/base/lreference.cpp
index c2559a616e..4f03887311 100644
--- a/plugins/ecmascript/es2panda/compiler/base/lreference.cpp
+++ b/plugins/ecmascript/es2panda/compiler/base/lreference.cpp
@@ -42,6 +42,10 @@ LReference::LReference(const ir::AstNode *node, PandaGen *pg, bool isDeclaration
 
     if (memberExpr->Object()->IsSuperExpression()) {
         refKind_ = ReferenceKind::SUPER;
+    } else if (memberExpr->IsPrivateReference()) {
+        refKind_ = ReferenceKind::PRIVATE;
+        privateCtor_ = pg_->AllocReg();
+        Function::LoadClassContexts(node_, pg_, privateCtor_, memberExpr->Property()->AsIdentifier()->Name());
     }
 
     obj_ = pg_->AllocReg();
diff --git a/plugins/ecmascript/es2panda/compiler/core/compilerContext.h b/plugins/ecmascript/es2panda/compiler/core/compilerContext.h
index 417b46f743..30ddba6768 100644
--- a/plugins/ecmascript/es2panda/compiler/core/compilerContext.h
+++ b/plugins/ecmascript/es2panda/compiler/core/compilerContext.h
@@ -19,6 +19,7 @@
 #include "macros.h"
 #include "mem/arena_allocator.h"
 #include "plugins/ecmascript/es2panda/es2panda.h"
+#include "plugins/ecmascript/es2panda/compiler/base/literals.h"
 
 #include <cstdint>
 #include <mutex>
@@ -53,23 +54,17 @@ public:
         return emitter_.get();
     }
 
-    int32_t AddContextLiteral(std::vector<const compiler::Literal *> &&literals)
+    int32_t AddContextLiteral(LiteralBuffer &&literals)
     {
-        std::lock_guard lock(m_);
         buffStorage_.emplace_back(std::move(literals));
-        return literalBufferIdx_++;
+        return buffStorage_.size() - 1;
     }
 
-    const std::vector<std::vector<const compiler::Literal *>> &ContextLiterals() const
+    const std::vector<LiteralBuffer> &ContextLiterals() const
     {
         return buffStorage_;
     }
 
-    std::mutex &Mutex()
-    {
-        return m_;
-    }
-
     bool IsDebug() const
     {
         return options_.isDebug;
@@ -92,10 +87,8 @@ public:
 
 private:
     binder::Binder *binder_;
-    int32_t literalBufferIdx_ {0};
-    std::vector<std::vector<const compiler::Literal *>> buffStorage_;
+    std::vector<LiteralBuffer> buffStorage_;
     std::unique_ptr<Emitter> emitter_;
-    std::mutex m_;
     CompilerOptions options_;
 };
 }  // namespace panda::es2panda::compiler
diff --git a/plugins/ecmascript/es2panda/compiler/core/compilerImpl.cpp b/plugins/ecmascript/es2panda/compiler/core/compilerImpl.cpp
index 6124d8e7f6..63e72362ab 100644
--- a/plugins/ecmascript/es2panda/compiler/core/compilerImpl.cpp
+++ b/plugins/ecmascript/es2panda/compiler/core/compilerImpl.cpp
@@ -36,18 +36,20 @@ CompilerImpl::~CompilerImpl()
 panda::pandasm::Program *CompilerImpl::Compile(CompilerContext *context, parser::Program *program,
                                                const es2panda::CompilerOptions &options)
 {
+    auto *emitter = context->GetEmitter();
+
     uint32_t index = 0;
     for (const auto &buff : context->ContextLiterals()) {
-        context->GetEmitter()->AddLiteralBuffer(buff, index++);
+        emitter->AddLiteralBuffer(buff, index++);
     }
 
+    emitter->LiteralBufferIndex() += context->ContextLiterals().size();
+
     if (program->Extension() == ScriptExtension::AS) {
         /* TODO(): AS files are not yet compiled */
         return nullptr;
     }
 
-    auto *emitter = context->GetEmitter();
-
     if (program->Extension() == ScriptExtension::TS) {
         ArenaAllocator localAllocator(SpaceType::SPACE_TYPE_COMPILER, nullptr, true);
         auto checker = std::make_unique<checker::Checker>(&localAllocator, context->Binder());
diff --git a/plugins/ecmascript/es2panda/compiler/core/emitter.cpp b/plugins/ecmascript/es2panda/compiler/core/emitter.cpp
index 0fdd7e9990..62268d50dd 100644
--- a/plugins/ecmascript/es2panda/compiler/core/emitter.cpp
+++ b/plugins/ecmascript/es2panda/compiler/core/emitter.cpp
@@ -77,6 +77,11 @@ static LiteralPair TransformLiteral(const compiler::Literal *literal)
             valueLit.value_ = literal->GetMethod();
             break;
         }
+        case compiler::LiteralTag::ASYNC_METHOD: {
+            valueLit.tag_ = panda_file::LiteralTag::ASYNCMETHOD;
+            valueLit.value_ = literal->GetMethod();
+            break;
+        }
         case compiler::LiteralTag::GENERATOR_METHOD: {
             valueLit.tag_ = panda_file::LiteralTag::GENERATORMETHOD;
             valueLit.value_ = literal->GetMethod();
@@ -358,78 +363,6 @@ void Emitter::GenESModuleModeRecord(bool isModule)
     prog_->record_table.emplace(modeRecord.name, std::move(modeRecord));
 }
 
-void Emitter::GenBufferLiterals(const LiteralBuffer &buff, uint32_t literalBufferIdx)
-{
-    std::vector<panda::pandasm::LiteralArray::Literal> literals;
-    literals.reserve(buff.size() * 2);
-
-    for (const auto &literal : buff) {
-        panda::pandasm::LiteralArray::Literal valueLit;
-        panda::pandasm::LiteralArray::Literal tagLit;
-
-        LiteralTag tag = literal.Tag();
-
-        switch (tag) {
-            case LiteralTag::BOOLEAN: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::BOOL;
-                valueLit.value_ = literal.GetBoolean();
-                break;
-            }
-            case LiteralTag::INTEGER: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::INTEGER;
-                valueLit.value_ = literal.GetInteger();
-                break;
-            }
-            case LiteralTag::DOUBLE: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::DOUBLE;
-                valueLit.value_ = literal.GetDouble();
-                break;
-            }
-            case LiteralTag::STRING: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::STRING;
-                valueLit.value_ = std::move(literal.GetString());
-                break;
-            }
-            case LiteralTag::ACCESSOR: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::ACCESSOR;
-                valueLit.value_ = static_cast<uint8_t>(0);
-                break;
-            }
-            case LiteralTag::METHOD: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::METHOD;
-                valueLit.value_ = std::move(literal.GetString());
-                break;
-            }
-            case LiteralTag::GENERATOR_METHOD: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::GENERATORMETHOD;
-                valueLit.value_ = std::move(literal.GetString());
-                break;
-            }
-            case LiteralTag::ASYNC_GENERATOR_METHOD: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::ASYNCGENERATORMETHOD;
-                valueLit.value_ = std::move(literal.GetString());
-                break;
-            }
-            case LiteralTag::NULL_VALUE: {
-                valueLit.tag_ = panda::panda_file::LiteralTag::NULLVALUE;
-                valueLit.value_ = static_cast<uint8_t>(0);
-                break;
-            }
-            default:
-                break;
-        }
-
-        tagLit.tag_ = panda::panda_file::LiteralTag::TAGVALUE;
-        tagLit.value_ = static_cast<uint8_t>(valueLit.tag_);
-
-        literals.emplace_back(tagLit);
-        literals.emplace_back(valueLit);
-    }
-
-    prog_->literalarray_table.emplace(std::to_string(literalBufferIdx),
-                                      panda::pandasm::LiteralArray(std::move(literals)));
-}
-
 static void UpdateLiteralBufferId(panda::pandasm::Ins *ins, uint32_t offset)
 {
     ins->imms.back() = std::get<int64_t>(ins->imms.back()) + offset;
@@ -441,7 +374,7 @@ void Emitter::AddProgramElement(ProgramElement *programElement)
 
     uint32_t newLiteralBufferIndex = literalBufferIndex_;
     for (const auto &buff : programElement->buffStorage) {
-        GenBufferLiterals(buff, newLiteralBufferIndex++);
+        AddLiteralBuffer(buff, newLiteralBufferIndex++);
     }
 
     for (auto *ins : programElement->literalBufferIns) {
@@ -489,12 +422,12 @@ void Emitter::DumpAsm(const pandasm::Program *prog)
     ss << std::endl;
 }
 
-void Emitter::AddLiteralBuffer(const std::vector<const compiler::Literal *> literals, uint32_t index)
+void Emitter::AddLiteralBuffer(const LiteralBuffer &literals, uint32_t index)
 {
     std::vector<pandasm::LiteralArray::Literal> literalArray;
 
-    for (const auto *literal : literals) {
-        auto [tagLit, valueLit] = TransformLiteral(literal);
+    for (const auto &literal : literals) {
+        auto [tagLit, valueLit] = TransformLiteral(&literal);
         literalArray.emplace_back(tagLit);
         literalArray.emplace_back(valueLit);
     }
diff --git a/plugins/ecmascript/es2panda/compiler/core/emitter.h b/plugins/ecmascript/es2panda/compiler/core/emitter.h
index 4162f3cd0b..b8c3a4ff6a 100644
--- a/plugins/ecmascript/es2panda/compiler/core/emitter.h
+++ b/plugins/ecmascript/es2panda/compiler/core/emitter.h
@@ -87,13 +87,17 @@ public:
     NO_MOVE_SEMANTIC(Emitter);
 
     void AddFunction(FunctionEmitter *func);
-    void AddLiteralBuffer(std::vector<const compiler::Literal *> literals, uint32_t index);
+    void AddLiteralBuffer(const LiteralBuffer &literals, uint32_t index);
     void AddProgramElement(ProgramElement *programElement);
     static void DumpAsm(const panda::pandasm::Program *prog);
     panda::pandasm::Program *Finalize(bool dumpDebugInfo);
 
+    uint32_t &LiteralBufferIndex()
+    {
+        return literalBufferIndex_;
+    }
+
 private:
-    void GenBufferLiterals(const LiteralBuffer &buff, uint32_t literalBufferIdx);
     void GenESAnnoatationRecord();
     void GenESModuleModeRecord(bool isModule);
 
diff --git a/plugins/ecmascript/es2panda/compiler/core/function.cpp b/plugins/ecmascript/es2panda/compiler/core/function.cpp
index 0666cea880..f5db62b757 100644
--- a/plugins/ecmascript/es2panda/compiler/core/function.cpp
+++ b/plugins/ecmascript/es2panda/compiler/core/function.cpp
@@ -162,6 +162,12 @@ void Function::CompileInstanceFields(PandaGen *pg, const ir::ScriptFunction *dec
         pg->ClassPrivateMethodOrAccessorAdd(decl, ctor, thisReg);
     }
 
+    if (klass->HasComputedInstanceField()) {
+        computedInstanceFieldsArray = pg->AllocReg();
+        pg->LoadClassComputedInstanceFields(klass, ctor);
+        pg->StoreAccumulator(klass, computedInstanceFieldsArray);
+    }
+
     for (auto const &element : elements) {
         if (!element->IsClassProperty()) {
             continue;
@@ -189,13 +195,8 @@ void Function::CompileInstanceFields(PandaGen *pg, const ir::ScriptFunction *dec
 
         Operand key;
         if (prop->IsComputed()) {
-            if (computedInstanceFieldsIndex == 0) {
-                pg->LoadClassComputedInstanceFields(prop, ctor);
-                computedInstanceFieldsArray = pg->AllocReg();
-                pg->StoreAccumulator(prop, computedInstanceFieldsArray);
-            }
-
             VReg keyReg = pg->AllocReg();
+            pg->LoadAccumulator(prop, computedInstanceFieldsArray);
             pg->LoadObjByIndex(prop, computedInstanceFieldsIndex++);
             pg->StoreAccumulator(prop, keyReg);
             key = keyReg;
diff --git a/plugins/ecmascript/es2panda/compiler/core/pandagen.cpp b/plugins/ecmascript/es2panda/compiler/core/pandagen.cpp
index eeda49278d..0946286f9d 100644
--- a/plugins/ecmascript/es2panda/compiler/core/pandagen.cpp
+++ b/plugins/ecmascript/es2panda/compiler/core/pandagen.cpp
@@ -1368,9 +1368,9 @@ void PandaGen::ImportModule(const ir::AstNode *node, const util::StringView &nam
     sa_.Emit<EcmaImportmodule>(node, name);
 }
 
-void PandaGen::SetClassComputedFields(const ir::AstNode *node, VReg computedInstanceFieldArray)
+void PandaGen::SetClassComputedFields(const ir::AstNode *node, VReg classReg, VReg computedInstanceFieldArray)
 {
-    ra_.Emit<EcmaSetclasscomputedfields>(node, computedInstanceFieldArray);
+    ra_.Emit<EcmaSetclasscomputedfields>(node, classReg, computedInstanceFieldArray);
 }
 
 void PandaGen::DefineClassWithBuffer(const ir::AstNode *node, const util::StringView &ctorId, int32_t litIdx,
@@ -1562,7 +1562,7 @@ void PandaGen::SortCatchTables()
 
 Operand PandaGen::ToNamedPropertyKey(const ir::Expression *prop, bool isComputed)
 {
-    VReg res {0};
+    VReg res {IRNode::REG_START};
 
     if (!isComputed) {
         if (prop->IsIdentifier()) {
diff --git a/plugins/ecmascript/es2panda/compiler/core/pandagen.h b/plugins/ecmascript/es2panda/compiler/core/pandagen.h
index 9bd77cb5cc..ac2cff5bb7 100644
--- a/plugins/ecmascript/es2panda/compiler/core/pandagen.h
+++ b/plugins/ecmascript/es2panda/compiler/core/pandagen.h
@@ -322,7 +322,7 @@ public:
     void CreateObjectWithExcludedKeys(const ir::AstNode *node, VReg obj, VReg argStart, size_t argCount);
     void ThrowObjectNonCoercible(const ir::AstNode *node);
     void CloseIterator(const ir::AstNode *node, VReg iter);
-    void SetClassComputedFields(const ir::AstNode *node, VReg computedInstanceFieldArray);
+    void SetClassComputedFields(const ir::AstNode *node, VReg classReg, VReg computedInstanceFieldArray);
     void DefineClassWithBuffer(const ir::AstNode *node, const util::StringView &ctorId, int32_t litIdx, VReg lexenv,
                                VReg base);
     void LoadClassComputedInstanceFields(const ir::AstNode *node, VReg ctor);
diff --git a/plugins/ecmascript/es2panda/compiler/core/regAllocator.h b/plugins/ecmascript/es2panda/compiler/core/regAllocator.h
index 5dcb654bbe..9b07854cea 100644
--- a/plugins/ecmascript/es2panda/compiler/core/regAllocator.h
+++ b/plugins/ecmascript/es2panda/compiler/core/regAllocator.h
@@ -114,7 +114,7 @@ protected:
 
     inline bool IsRegisterCorrect(const VReg *reg) const
     {
-        return *reg < limit_;
+        return *reg >= IRNode::REG_START - limit_;
     }
 
     VReg Spill(IRNode *ins, VReg reg);
diff --git a/plugins/ecmascript/es2panda/compiler/templates/isa.h.erb b/plugins/ecmascript/es2panda/compiler/templates/isa.h.erb
index 0bc8b77ce5..f84ff4c16c 100644
--- a/plugins/ecmascript/es2panda/compiler/templates/isa.h.erb
+++ b/plugins/ecmascript/es2panda/compiler/templates/isa.h.erb
@@ -212,7 +212,7 @@ public:
 % end
 % for imm in op_map['imm']
       ins->imms.emplace_back(<%= imm %>);
-% if insn.properties.include? 'literalarray_id'
+% if insn.properties.include? 'literalarray_id' and insn.properties.none? 'skip_literal_id_patch'
       programElement->literalBufferIns.push_back(ins);
 %end
 % end
diff --git a/plugins/ecmascript/es2panda/ir/base/classDefinition.cpp b/plugins/ecmascript/es2panda/ir/base/classDefinition.cpp
index 6879825b76..3dd4dfc1f9 100644
--- a/plugins/ecmascript/es2panda/ir/base/classDefinition.cpp
+++ b/plugins/ecmascript/es2panda/ir/base/classDefinition.cpp
@@ -50,6 +50,14 @@ bool ClassDefinition::HasPrivateMethod() const
                        [](const auto *element) { return element->IsMethodDefinition() && element->IsPrivate(); });
 }
 
+bool ClassDefinition::HasComputedInstanceField() const
+{
+    return std::any_of(body_.cbegin(), body_.cend(), [](const auto *element) {
+        return element->IsClassProperty() && element->IsComputed() &&
+               !(element->Modifiers() & ir::ModifierFlags::STATIC);
+    });
+}
+
 bool ClassDefinition::HasMatchingPrivateKey(const util::StringView &name) const
 {
     return std::any_of(body_.cbegin(), body_.cend(), [&name](const auto *element) {
@@ -127,19 +135,17 @@ void ClassDefinition::InitializeClassName(compiler::PandaGen *pg) const
 }
 
 // NOLINTNEXTLINE(google-runtime-references)
-std::tuple<int32_t, compiler::LiteralBuffer *> ClassDefinition::CreateClassStaticProperties(
-    compiler::PandaGen *pg, util::BitSet &compiled) const
+static std::tuple<int32_t, compiler::LiteralBuffer> CreateClassStaticProperties(
+    compiler::PandaGen *pg, util::BitSet &compiled, const ArenaVector<ClassElement *> &properties)
 {
     compiler::LiteralBuffer buf {};
-    compiler::LiteralBuffer *privatebuf {};
+    compiler::LiteralBuffer privatebuf {};
     compiler::LiteralBuffer staticBuf {};
     bool seenComputed = false;
     std::unordered_map<util::StringView, size_t> propNameMap;
     std::unordered_map<util::StringView, size_t> staticPropNameMap;
     int32_t computedInstanceFields = 0;
 
-    const auto &properties = body_;
-
     for (size_t i = 0; i < properties.size(); i++) {
         const ir::ClassElement *prop = properties[i];
 
@@ -150,8 +156,8 @@ std::tuple<int32_t, compiler::LiteralBuffer *> ClassDefinition::CreateClassStati
         if (prop->IsClassProperty()) {
             bool isStatic = (prop->AsClassProperty()->Modifiers() & ir::ModifierFlags::STATIC) != 0;
             if (prop->IsPrivate()) {
-                privatebuf->emplace_back(pg->Allocator()->New<NumberLiteral>(static_cast<double>(prop->ToPrivateFieldKind(isStatic))));
-                privatebuf->emplace_back(pg->Allocator()->New<StringLiteral>(prop->Key()->AsIdentifier()->Name()));
+                privatebuf.emplace_back(static_cast<uint32_t>(prop->ToPrivateFieldKind(isStatic)));
+                privatebuf.emplace_back(prop->Key()->AsIdentifier()->Name());
                 continue;
             }
 
@@ -176,9 +182,8 @@ std::tuple<int32_t, compiler::LiteralBuffer *> ClassDefinition::CreateClassStati
         auto &nameMap = prop->IsStatic() ? staticPropNameMap : propNameMap;
 
         if (prop->IsPrivate()) {
-            privatebuf->emplace_back(pg->Allocator()->New<NumberLiteral>(
-                static_cast<double>(prop->ToPrivateFieldKind(propMethod->IsStatic()))));
-            privatebuf->emplace_back(pg->Allocator()->New<StringLiteral>(name));
+            privatebuf.emplace_back(static_cast<uint32_t>(prop->ToPrivateFieldKind(propMethod->IsStatic())));
+            privatebuf.emplace_back(name);
 
             const ir::ScriptFunction *func = propMethod->Value()->AsFunctionExpression()->Function();
 
@@ -193,7 +198,7 @@ std::tuple<int32_t, compiler::LiteralBuffer *> ClassDefinition::CreateClassStati
                 tag = compiler::LiteralTag::GENERATOR_METHOD;
             }
 
-            privatebuf->emplace_back(new compiler::Literal(tag, func->Scope()->InternalName()));
+            privatebuf.emplace_back(tag, func->Scope()->InternalName());
             compiled.Set(i);
             continue;
         }
@@ -259,6 +264,12 @@ void ClassDefinition::CompileMissingProperties(compiler::PandaGen *pg, const uti
     pg->LoadObjByName(this, "prototype");
     pg->StoreAccumulator(this, protoReg);
 
+    if (HasComputedInstanceField()) {
+        pg->CreateEmptyArray(this);
+        computedInstanceFieldsArray = pg->AllocReg();
+        pg->StoreAccumulator(this, computedInstanceFieldsArray);
+    }
+
     for (size_t i = 0; i < properties.size(); i++) {
         if (compiled.Test(i)) {
             continue;
@@ -331,18 +342,12 @@ void ClassDefinition::CompileMissingProperties(compiler::PandaGen *pg, const uti
             continue;
         }
 
-        if (computedInstanceFieldsIndex == 0) {
-            pg->CreateEmptyArray(this);
-            pg->StoreAccumulator(this, computedInstanceFieldsArray);
-        }
-
         pg->LoadPropertyKeyAcc(prop->Key(), prop->IsComputed());
         pg->StOwnByIndex(this, computedInstanceFieldsArray, computedInstanceFieldsIndex++);
     }
 
     if (computedInstanceFieldsIndex != 0) {
-        pg->LoadAccumulator(this, classReg);
-        pg->SetClassComputedFields(this, computedInstanceFieldsArray);
+        pg->SetClassComputedFields(this, classReg, computedInstanceFieldsArray);
     }
 
     CompileStaticFieldInitializers(pg, classReg, staticComputedFieldKeys);
@@ -424,10 +429,22 @@ void ClassDefinition::Compile(compiler::PandaGen *pg) const
     util::StringView ctorId = ctor_->Function()->Scope()->InternalName();
     util::BitSet compiled(body_.size());
 
-    auto [bufIdx, privateBuf] = CreateClassStaticProperties(pg, compiled);
+    auto [bufIdx, privateBuf] = CreateClassStaticProperties(pg, compiled, body_);
 
     pg->DefineClassWithBuffer(this, ctorId, bufIdx, lexenv, baseReg);
     pg->StoreAccumulator(this, classReg);
+
+    if (!privateBuf.empty()) {
+        pg->DefineClassPrivateFields(this, pg->AddLiteralBuffer(std::move(privateBuf)));
+    }
+
+    binder::ScopeFindResult res = pg->Scope()->Find(privateId_);
+    ASSERT(res.variable);
+
+    if (res.variable->AsLocalVariable()->LexicalBound()) {
+        pg->StoreLexicalVar(this, res.lexLevel, res.variable->AsLocalVariable()->LexIdx());
+    }
+
     InitializeClassName(pg);
 
     CompileMissingProperties(pg, compiled, classReg);
diff --git a/plugins/ecmascript/es2panda/ir/base/classDefinition.h b/plugins/ecmascript/es2panda/ir/base/classDefinition.h
index df9e1a0159..dfd0e267fa 100644
--- a/plugins/ecmascript/es2panda/ir/base/classDefinition.h
+++ b/plugins/ecmascript/es2panda/ir/base/classDefinition.h
@@ -118,6 +118,7 @@ public:
 
     const FunctionExpression *Ctor() const;
     bool HasPrivateMethod() const;
+    bool HasComputedInstanceField() const;
     bool HasMatchingPrivateKey(const util::StringView &name) const;
 
     void Iterate(const NodeTraverser &cb) const override;
@@ -128,8 +129,6 @@ public:
 private:
     compiler::VReg CompileHeritageClause(compiler::PandaGen *pg) const;
     void InitializeClassName(compiler::PandaGen *pg) const;
-    std::tuple<int32_t, compiler::LiteralBuffer *> CreateClassStaticProperties(compiler::PandaGen *pg,
-                                                                               util::BitSet &compiled) const;
     void CompileMissingProperties(compiler::PandaGen *pg, const util::BitSet &compiled, compiler::VReg classReg) const;
     void CompileStaticFieldInitializers(compiler::PandaGen *pg, compiler::VReg classReg,
                                         const std::vector<compiler::VReg> &staticComputedFieldKeys) const;
diff --git a/plugins/ecmascript/es2panda/ir/expressions/identifier.h b/plugins/ecmascript/es2panda/ir/expressions/identifier.h
index 4daa88e71f..d615185627 100644
--- a/plugins/ecmascript/es2panda/ir/expressions/identifier.h
+++ b/plugins/ecmascript/es2panda/ir/expressions/identifier.h
@@ -146,6 +146,11 @@ public:
         return variable_;
     }
 
+    void AddDecorators([[maybe_unused]] ArenaVector<ir::Decorator *> &&decorators) override
+    {
+        decorators_ = std::move(decorators);
+    }
+
     void Iterate(const NodeTraverser &cb) const override;
     void Dump(ir::AstDumper *dumper) const override;
     void Compile([[maybe_unused]] compiler::PandaGen *pg) const override;
diff --git a/plugins/ecmascript/es2panda/ir/expressions/memberExpression.cpp b/plugins/ecmascript/es2panda/ir/expressions/memberExpression.cpp
index 10f94be9e7..67b225626b 100644
--- a/plugins/ecmascript/es2panda/ir/expressions/memberExpression.cpp
+++ b/plugins/ecmascript/es2panda/ir/expressions/memberExpression.cpp
@@ -53,6 +53,13 @@ void MemberExpression::LoadRhs(compiler::PandaGen *pg) const
 
     if (isSuper) {
         pg->LoadSuperProperty(this, prop);
+    } else if (IsPrivateReference()) {
+        const auto &name = property_->AsIdentifier()->Name();
+        compiler::VReg objReg = pg->AllocReg();
+        pg->StoreAccumulator(this, objReg);
+        compiler::VReg ctor = pg->AllocReg();
+        compiler::Function::LoadClassContexts(this, pg, ctor, name);
+        pg->ClassPrivateFieldGet(this, ctor, objReg, name);
     } else {
         pg->LoadObjProperty(this, prop);
     }
diff --git a/plugins/ecmascript/isa/isa.yaml b/plugins/ecmascript/isa/isa.yaml
index 0874608f28..4f91ca1a12 100644
--- a/plugins/ecmascript/isa/isa.yaml
+++ b/plugins/ecmascript/isa/isa.yaml
@@ -26,6 +26,8 @@ properties:
     description: Handler is implemented in IRtoC and can be inlined by JIT/AOT.
   - tag: use_ic
     description: Instruction uses inline cache
+  - tag: skip_literal_id_patch
+    description: Do not patch literal array id
 
 groups:
   - title: Ecma extension intrunctions
@@ -927,10 +929,10 @@ groups:
         format: [pref_op_v_8]
         intrinsic_name: INTRINSIC_LOAD_CLASS_COMPUTED_INSTANCE_FIELDS
 
-      - sig: ecma.setclasscomputedfields v:in:top
-        acc: in:top
+      - sig: ecma.setclasscomputedfields v1:in:top, v2:in:top
+        acc: none
         prefix: ecma
-        format: [pref_op_v_8]
+        format: [pref_op_v1_8_v2_8]
         intrinsic_name: INTRINSIC_SET_CLASS_COMPUTED_FIELDS
 
       - sig: ecma.stlexvardyn imm1, imm2
@@ -984,6 +986,7 @@ groups:
         prefix: ecma
         format: [pref_op_imm_16_v_8]
         intrinsic_name: INTRINSIC_DEFINE_CLASS_PRIVATE_FIELDS
+        properties: [literalarray_id]
 
       - sig: ecma.classprivatemethodoraccessoradd v1:in:top, v2:in:top
         acc: none
@@ -1169,6 +1172,7 @@ groups:
         prefix: ecma
         format: [pref_op_imm_16]
         intrinsic_name: INTRINSIC_LD_EVAL_BINDINGS
+        properties: [literalarray_id, skip_literal_id_patch]
 
       - sig: ecma.directeval imm, v1:in:top, v2:in:top
         acc: out:top
diff --git a/plugins/ecmascript/runtime/class_info_extractor.cpp b/plugins/ecmascript/runtime/class_info_extractor.cpp
index 2d1f4c137c..d4e1ac3e25 100644
--- a/plugins/ecmascript/runtime/class_info_extractor.cpp
+++ b/plugins/ecmascript/runtime/class_info_extractor.cpp
@@ -258,7 +258,7 @@ JSHandle<JSHClass> ClassInfoExtractor::CreateConstructorHClass(JSThread *thread,
         hclass->SetNumberOfProps(length);
     } else {
         // dictionary mode
-        hclass = factory->NewEcmaDynClass(JSFunction::SIZE, JSType::JS_FUNCTION, 0);  // without in-obj
+        hclass = factory->NewEcmaDynClass(JSConstructorFunction::SIZE, JSType::JS_FUNCTION, 0);  // without in-obj
         hclass->SetIsDictionaryMode(true);
         hclass->SetNumberOfProps(0);
     }
diff --git a/plugins/ecmascript/runtime/class_linker/panda_file_translator.cpp b/plugins/ecmascript/runtime/class_linker/panda_file_translator.cpp
index f3f843427a..1734d63fa1 100644
--- a/plugins/ecmascript/runtime/class_linker/panda_file_translator.cpp
+++ b/plugins/ecmascript/runtime/class_linker/panda_file_translator.cpp
@@ -477,7 +477,7 @@ void PandaFileTranslator::TranslateBytecode(uint32_t insSz, const uint8_t *insAr
                     FixInstructionId32(bcIns, index);
                     break;
                 case BytecodeInstruction::Opcode::ECMA_DEFINECLASSPRIVATEFIELDS_PREF_IMM16_V8: {
-                    index = GetOrInsertConstantPool(ConstPoolType::CLASS_LITERAL,
+                    index = GetOrInsertConstantPool(ConstPoolType::TAGGED_ARRAY,
                                                     bcIns.GetImm<BytecodeInstruction::Format::PREF_IMM16_V8>());
                     FixInstructionId32(bcIns, index);
                     break;
diff --git a/plugins/ecmascript/runtime/class_linker/panda_file_translator.h b/plugins/ecmascript/runtime/class_linker/panda_file_translator.h
index 73745855a6..4538804d03 100644
--- a/plugins/ecmascript/runtime/class_linker/panda_file_translator.h
+++ b/plugins/ecmascript/runtime/class_linker/panda_file_translator.h
@@ -51,7 +51,6 @@ private:
         ASYNC_FUNCTION,
         ASYNC_GENERATOR_FUNCTION,
         CLASS_FUNCTION,
-        CLASS_LITERAL,
         METHOD,
         ARRAY_LITERAL,
         OBJECT_LITERAL,
diff --git a/plugins/ecmascript/runtime/ecma_runtime.yaml b/plugins/ecmascript/runtime/ecma_runtime.yaml
index bd7eca94db..356364d26a 100644
--- a/plugins/ecmascript/runtime/ecma_runtime.yaml
+++ b/plugins/ecmascript/runtime/ecma_runtime.yaml
@@ -1655,7 +1655,7 @@ intrinsics:
   exception: true
   signature:
     ret: void
-    args: [acc, any]
+    args: [any, any]
   impl: panda::ecmascript::intrinsics::SetClassComputedFields
 
 - name: DefineClassWithBuffer
diff --git a/plugins/ecmascript/runtime/interpreter/ecma-interpreter-inl.h b/plugins/ecmascript/runtime/interpreter/ecma-interpreter-inl.h
index 8cf70a4aa9..a5bada4e85 100644
--- a/plugins/ecmascript/runtime/interpreter/ecma-interpreter-inl.h
+++ b/plugins/ecmascript/runtime/interpreter/ecma-interpreter-inl.h
@@ -2135,13 +2135,14 @@ public:
     ALWAYS_INLINE void HandleEcmaSetclasscomputedfields()
     {
         auto v0 = this->GetInst().template GetVReg<format, 0>();
+        auto v1 = this->GetInst().template GetVReg<format, 1>();
         LOG_INST() << "setclasscomputedfields"
-                   << " v:" << v0;
+                   << " class_reg v:" << v0 << " computed_fields v:" << v1;
 
-        uint64_t acc = GetAccAsTaggedValue().GetRawData();
-        uint64_t computedFields = GetRegAsTaggedValue(v0).GetRawData();
+        uint64_t classReg = GetRegAsTaggedValue(v0).GetRawData();
+        uint64_t computedFields = GetRegAsTaggedValue(v1).GetRawData();
 
-        intrinsics::SetClassComputedFields(this->GetJSThread(), acc, computedFields);
+        intrinsics::SetClassComputedFields(this->GetJSThread(), classReg, computedFields);
         this->template MoveToNextInst<format, true>();
     }
 
diff --git a/plugins/ecmascript/runtime/interpreter/interpreter.h b/plugins/ecmascript/runtime/interpreter/interpreter.h
index d6a8889cb8..36c55741e8 100644
--- a/plugins/ecmascript/runtime/interpreter/interpreter.h
+++ b/plugins/ecmascript/runtime/interpreter/interpreter.h
@@ -223,7 +223,7 @@ enum EcmaOpcode {
     CREATEOBJECTHAVINGMETHOD_IMM8_IMM16,
     THROWIFSUPERNOTCORRECTCALL_IMM8_IMM16,
     LOADCLASSCOMPUTEDINSTANCEFIELDS_IMM8_V8,
-    SETCLASSCOMPUTEDFIELDS_IMM8_V8,
+    SETCLASSCOMPUTEDFIELDS_IMM8_V8_V8,
     DEFINECLASSWITHBUFFER_IMM8_ID16_IMM16_V8_V8,
     RETURN_DYN,
     MOV_V4_V4,
diff --git a/plugins/ecmascript/runtime/intrinsics-inl.h b/plugins/ecmascript/runtime/intrinsics-inl.h
index 26eaf3404e..ab90c38b29 100644
--- a/plugins/ecmascript/runtime/intrinsics-inl.h
+++ b/plugins/ecmascript/runtime/intrinsics-inl.h
@@ -1958,22 +1958,22 @@ INLINE_ECMA_INTRINSICS uint64_t DefineClassWithBuffer(JSThread *thread, uint32_t
 // NOLINTNEXTLINE(misc-definitions-in-headers)
 INLINE_ECMA_INTRINSICS void SetClassComputedFields(JSThread *thread, uint64_t class_reg, uint64_t computed_fields)
 {
-    JSTaggedValue cls(class_reg);
-    ASSERT(cls.IsClassConstructor());
-    JSConstructorFunction *clsCtor = JSConstructorFunction::Cast(cls.GetTaggedObject());
+    [[maybe_unused]] EcmaHandleScope handleScope(thread);
 
-    clsCtor->SetComputedFields(thread, JSTaggedValue(computed_fields));
+    JSHandle<JSConstructorFunction> cls(thread, JSTaggedValue(class_reg));
+    JSHandle<JSArray> computedFieldsHandle(thread, JSTaggedValue(computed_fields));
+    cls->SetComputedFields(thread, computedFieldsHandle);
 }
 
 // NOLINTNEXTLINE(misc-definitions-in-headers)
 INLINE_ECMA_INTRINSICS uint64_t LoadClassComputedInstanceFields(JSThread *thread, uint64_t class_reg)
 {
-    JSTaggedValue cls(class_reg);
-    ASSERT(cls.IsClassConstructor());
-    JSConstructorFunction *clsCtor = JSConstructorFunction::Cast(cls.GetTaggedObject());
+    [[maybe_unused]] EcmaHandleScope handleScope(thread);
 
-    JSTaggedValue computedInstanceFields = clsCtor->GetComputedFields();
-    clsCtor->SetComputedFields(thread, JSTaggedValue::Hole());
+    JSHandle<JSConstructorFunction> cls(thread, JSTaggedValue(class_reg));
+    JSTaggedValue computedInstanceFields = cls->GetComputedFields();
+    ASSERT(computedInstanceFields.IsJSArray());
+    cls->SetComputedFields(thread, JSTaggedValue::Hole());
 
     return computedInstanceFields.GetRawData();
 }
-- 
2.17.1

