/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "utils/utf.h"
#include "gtest/gtest.h"
#include "os/failure_retry.h"
#include "os/mutex.h"
#include "os/thread.h"

#ifdef PANDA_TARGET_WINDOWS
#include <processthreadsapi.h>
#include <windows.h>
#include <stdio.h>
#include <process.h>
#endif

namespace panda::os::thread {
class ThreadTest : public testing::Test {};

uint32_t thread_id_ = 0;
panda::os::memory::Mutex mu;

#ifdef PANDA_TARGET_UNIX
// On linux, the priority can be set within range [-20, 19], and 19 is the lowest priority.
constexpr int LOWER_PRIOIRITY = 1;
constexpr int LOWEST_PRIORITY = 19;
#elif defined(PANDA_TARGET_WINDOWS)
// On Windows, the priority can be set within range [-2, 2], and -2 is the lowest priority.
constexpr int LOWER_PRIOIRITY = -1;
constexpr int LOWEST_PRIORITY = -2;
#endif

void ThreadFunc(int sleep_time)
{
    mu.Lock();
    thread_id_ = GetCurrentThreadId();
    mu.Unlock();
    // Waiting for SetPriority/SetThreadName operations
    NativeSleep(sleep_time);
}

TEST_F(ThreadTest, ThreadStartTest)
{
    auto thread_id = ThreadStart(ThreadFunc, 200);
    // Waiting for thread_id_ updates to the new thread id
    void *ret = nullptr;
    ThreadJoin(thread_id, &ret);
    mu.Lock();
    ASSERT(GetCurrentThreadId() != thread_id_);
    mu.Unlock();
}

TEST_F(ThreadTest, SetCurrentThreadPriorityTest)
{
    // Since setting higher priority needs "sudo" right, we only test lower one here.
    auto ret1 = SetPriority(GetCurrentThreadId(), LOWER_PRIOIRITY);
    ASSERT_EQ(ret1, 0);

    auto prio1 = GetPriority(GetCurrentThreadId());
    ASSERT_EQ(prio1, LOWER_PRIOIRITY);

    auto ret2 = SetPriority(GetCurrentThreadId(), LOWEST_PRIORITY);
    ASSERT_EQ(ret2, 0);

    auto prio2 = GetPriority(GetCurrentThreadId());
    ASSERT_EQ(prio2, LOWEST_PRIORITY);
}

TEST_F(ThreadTest, SetOtherThreadPriorityTest)
{
    auto parent_pid = GetCurrentThreadId();
    auto parent_prio_before = GetPriority(parent_pid);

    auto new_thread = ThreadStart(ThreadFunc, 200);
    // Waiting for thread_id_ updates to the new thread id, and before the new thread exited
    NativeSleep(100);
    mu.Lock();
    auto child_pid = thread_id_;
    mu.Unlock();

    auto child_prio_before = GetPriority(child_pid);
    auto ret = SetPriority(child_pid, LOWEST_PRIORITY);
    ASSERT_EQ(ret, 0);

    auto child_prio_after = GetPriority(child_pid);
    auto parent_prio_after = GetPriority(parent_pid);

    void *res;
    ThreadJoin(new_thread, &res);

    ASSERT_EQ(parent_prio_before, parent_prio_after);
#ifdef PANDA_TARGET_UNIX
    ASSERT(child_prio_before <= child_prio_after);
#elif defined(PANDA_TARGET_WINDOWS)
    ASSERT(child_prio_after <= child_prio_before);
#endif
}
}  // namespace panda::os::thread
